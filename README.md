# Избранные материалы курса "Основы машинного обучения" ВШПИ МФТИ

## Общая информация

Автор решений: Шубин Владислав Андреевич

## Навигация

- [Практические задачи](/Tasks/Practice/)
- [Теоретические задачи](/Tasks/Theory/)

## Полезные ссылки

- [Учебник ШАДа](https://education.yandex.ru/handbook/ml)
- [Визуализация K-means](https://www.naftaliharris.com/blog/visualizing-k-means-clustering/)
- [Песочница-тренажер TensorFlow](https://playground.tensorflow.org/)
- [Double Q-learning](https://proceedings.neurips.cc/paper/2010/file/091d584fced301b442654dd8c23b3fc9-Paper.pdf)